﻿using UnityEngine;
using System.Collections;

public class AnimationSpriteSheet : MonoBehaviour {
	public int uvX = 4;
	public int uvY = 2;
	public float fps = 24.0f;
	
	void Update()
	{
		int index = (int)(Time.time * fps);
		
		index = index % (uvX * uvY);
		
		Vector2 size = new Vector2(1.0f / uvX, 1.0f / uvY);
		
		float uIndex = index % uvX;
		float vIndex = index / uvX;
		Vector2 offset = new Vector2(uIndex * size.x, 1.0f - size.y - vIndex * size.y);

		renderer.material.SetTextureOffset ("_MainTex", offset);
		renderer.material.SetTextureScale ("_MainTex", size);
	}
}
