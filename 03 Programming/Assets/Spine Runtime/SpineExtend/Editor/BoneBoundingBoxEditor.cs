﻿

using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
//using GMT;

[CustomEditor(typeof(BoneBoundingBox))]
public class BoneBoundingBoxEditor: Editor {

	BoneBoundingBox syncher;
	
	public List<string> actionTypeList = new List<string>();
	public int actionTypeIdx;
	public List<string> actionNameList = new List<string>();
	public int actionNameIdx;

	public List<string> boneNameList = new List<string>();

	bool isPlay;

	string boneNameTemp = "", spriteNameTemp = "";
	void OnEnable(){

		syncher = (BoneBoundingBox)target;

		if (syncher.skAnimation == null)
			syncher.skAnimation = syncher.transform.parent.GetComponent<SkeletonAnimationExtend> ();

		if( syncher.boxCollider2D == null ){
			syncher.CreateBoxCollider();
		}


	}
	
	void OnDisable(){
	}

	public override void OnInspectorGUI(){	
		
		if( ! syncher.gameObject.activeInHierarchy ) return;

		syncher.skAnimation = EditorGUILayout.ObjectField("SK Animation Comp" , syncher.skAnimation, typeof( SkeletonAnimationExtend ), true) as SkeletonAnimationExtend;
		
		if (syncher.skAnimation != null && syncher.skAnimation.skeleton != null) {

			spriteNameTemp = syncher.spriteName;
			boneNameTemp = syncher.boneName;

			// Initial skin name.
			string[] skins = new string[syncher.skAnimation.skeleton.Data.Skins.Count];
			int skinIndex = 0;
			for (int i = 0; i < skins.Length; i++) {
				string name = syncher.skAnimation.skeleton.Data.Skins[i].Name;
				skins[i] = name;
				if (name == syncher.skAnimation.initialSkinName)
					skinIndex = i;
			}
			
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField("Initial Skin & Clip");
			EditorGUIUtility.LookLikeControls();
			skinIndex = EditorGUILayout.Popup(skinIndex, skins);
			
			
			syncher.skAnimation.initialSkinName = skins[skinIndex];
			
			// Animation name.
			string[] animations = new string[syncher.skAnimation.skeleton.Data.Animations.Count + 1];
			animations[0] = "<None>";
			int animationIndex = 0;
			for (int i = 0; i < animations.Length - 1; i++) {
				string name = syncher.skAnimation.skeleton.Data.Animations[i].Name;
				animations[i + 1] = name;
				if (name == syncher.skAnimation.AnimationName)
					animationIndex = i + 1;
			}
			
			
			EditorGUIUtility.LookLikeControls();
			int temp = animationIndex;
			
			animationIndex = EditorGUILayout.Popup(animationIndex, animations);
			
			if( animationIndex != temp ){
				if (animationIndex == 0)
					syncher.skAnimation.AnimationName = null;
				else
					syncher.skAnimation.AnimationName = animations[animationIndex];
			}
			
			EditorGUILayout.EndHorizontal();

			int bLen = syncher.skAnimation.skeleton.Bones.Count;
			string[] boneNames = new string[bLen];
			int boneIdx = 0;

			for( int i = 0; i < bLen; i++){
				string name = syncher.skAnimation.skeleton.Bones[i].Data.Name;
				boneNames[i] = name;
				if( syncher.boneName == name ) boneIdx = i;
			}

			boneIdx = EditorGUILayout.Popup("Bone Name", boneIdx, boneNames );

			if( syncher.boneName != boneNames[ boneIdx ] ){
				boneNameTemp = boneNames[ boneIdx ];
			}

			if( syncher.boxCollider2D == null ) syncher.CreateBoxCollider();

			tk2dSpriteCollectionData tkData = syncher.skAnimation.skeletonDataAsset.spriteCollection;
			int spCount = tkData.Count;

			string[] spriteNames = new string[ spCount ];

			int spriteNidx = 0; 

			for( int i = 0; i < spCount; i++){
				spriteNames[i] = tkData.spriteDefinitions[i].name;
				if( syncher.spriteName == spriteNames[i] ) spriteNidx = i;
			}

			spriteNidx = EditorGUILayout.Popup("Box Sprite Name", spriteNidx, spriteNames );

			if( syncher.spriteName != spriteNames[ spriteNidx ] ){
				spriteNameTemp = spriteNames[ spriteNidx ];
				syncher.InitBoundingBox( boneNameTemp, spriteNameTemp );
				syncher.transform.localScale = Vector3.one;
			}

			syncher.imageBaseRotation = (int) EditorGUILayout.IntSlider("Image Base Rotation", (int)syncher.imageBaseRotation,0,360);

			syncher.useRealTimeBoundingUpdate = EditorGUILayout.Toggle("Use RealTime Bounding Update", syncher.useRealTimeBoundingUpdate );

		}
	
		{
			EditorGUILayout.Separator();
		}
		
		//update and redraw:
		if(GUI.changed){
			EditorUtility.SetDirty(syncher);	
			if( boneNameTemp.Length > 0 && spriteNameTemp.Length > 0 ){
				syncher.InitBoundingBox( boneNameTemp, spriteNameTemp );
				syncher.transform.localScale = Vector3.one;
				syncher.skAnimation.Update();
			}

		}
	}


	// Scene view 에서 해당 obejct 로 포커싱을 해주는 함수
	void MoveObject( GameObject go){			
		UnityEditor.Selection.activeObject = go;
		try {
			UnityEditor.SceneView.lastActiveSceneView.FrameSelected();	
		} catch (System.Exception ex) {
			Debug.Log( "move object exception is passed " + ex);
		}
	}
}
