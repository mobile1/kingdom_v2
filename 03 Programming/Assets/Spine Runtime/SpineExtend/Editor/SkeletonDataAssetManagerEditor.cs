﻿using UnityEditor;
using UnityEngine;
using System.Collections;
using System.IO;

[CustomEditor(typeof(SkeletonDataAssetManager))]
public class SkeletonDataAssetManagerEditor : Editor {

	SkeletonDataAssetManager _target;
	SkeletonDataAssetManager.ObjectFolder[] folders;
	
	GUIStyle style = new GUIStyle();
	
	void OnEnable(){
		//i like bold handle labels since I'm getting old:
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.white;
		_target = (SkeletonDataAssetManager)target;	
		folders = _target.folders;
	}
	public Object testKeyObject;
	public string testKey;
	
	public override void OnInspectorGUI(){		
		
		if( GUILayout.Button("Reset") ){
			ReloadObject();
		}
		
		if( Application.isPlaying ){
			GUILayout.BeginHorizontal();
			{
				testKeyObject = EditorGUILayout.ObjectField("test Object ", testKeyObject,typeof(Object),true);
				if( testKeyObject ) 
				{
					testKey = testKeyObject.name;
					if( GUILayout.Button("Create") ){
						_target.CreateObject(testKey);
					}
				}
			}
			GUILayout.EndHorizontal();
		}
		
		DrawDefaultInspector();
	}
	
	
	
	public void ReloadObject(){
		
		for(int fid = 0; fid < folders.Length; fid++){
			
			string path = AssetDatabase.GetAssetPath( folders[fid].folder );
			
			GameObject[] getObjects = GetAtPath<  GameObject >( path );
			
			folders[ fid ].objectInfos = new SkeletonDataAssetManager.ObjectInfo[ getObjects.Length ];
			
			folders[fid].name = path.Replace("Assets/","") + " " + getObjects.Length;
			
			// 해당 경로에 존재 하는 오브젝트를 읽어 들인다.
			for( int oid = 0; oid < getObjects.Length ; oid++){
				
				folders[ fid ].objectInfos[oid] = new SkeletonDataAssetManager.ObjectInfo( getObjects[ oid ] );
				
				folders[ fid ].objectInfos[oid].name = getObjects[ oid ].name;			
			}
		}	
	}
	
	
	public static T[] GetAtPath<T> (string path) {
		
		ArrayList al = new ArrayList();
		
		string [] fileEntries = Directory.GetFiles(path);
		
		foreach(string fileName in fileEntries)
		{
			int index = fileName.LastIndexOf("/");
			
			string localPath = path;
			
			if (index > 0)
				localPath += fileName.Substring(index);
			
			Object t = Resources.LoadAssetAtPath(localPath, typeof(T));
			if(t != null)
				al.Add(t);
		}
		
		T[] result = new T[al.Count];
		
		for(int i=0;i<al.Count;i++)
			result[i] = (T)al[i];
		
		return result;
	}
}
