﻿/* 
	테스트용 캐릭터 클래스

	*/


using UnityEngine;
using System.Collections;
using System;

public class SpineExtendCharacter : MonoBehaviour {
	// 캐릭터 타입 정의.
	public enum CharacterType{ General, Soldier, Max };
	// 스킨 정의.
	public enum SkinType{ basic, s2, s3, s4, s5, s6, Max }
	// 병과. 
	public enum AttackType{ a,b,c,d,e,Max };
	// 애니메이션 클립.
	public enum ClipType{atk,down,hit_01,hit_02,idle,run};

	// 병과와 애니 클립을 조합 하여 사용함.


	public CharacterType 	characterType;
	public SkinType 		skinType;
	public AttackType 		attackType;
	public ClipType 		currentClipType;

	// 움짐임 벡터
	public Vector3 moveVector = Vector3.zero;
	public float moveSpeed = 1000;
	Transform mytf;

	// 스파인 애니메이션.
	public SkeletonAnimationExtend skAnimation;

	// 공격등의 한번 실행 되고 마는 애니며이션 체크용 함수.
	public bool isOneShotAnimationning;

	// 테스트용 총알을 만드는 클래스.
	public SpineExtendCharacterBulletMaker bulletMaker;

	// 자동으로 Start함수에서 초기화 할 것인가.
	// 하드코딩으로 본 클래스를 생성 하는 경우 초기화 작업을 별도로 해줘야 하기에 다음 변수를 두어 중복처리를 막는다. 
	// 하이라키에 등록 해서 쓰는 경우에는 true, 소스상에서 생성 하는 경우에는 false.
	public bool autoInitSkeleton = false;

	// 본 캐릭터가 플레이어 캐릭터 인가.
	public bool isPlayer = false;

	// Use this for initialization
	void Start () {
		mytf = transform;
		// 자동 초기화 할경우 .
		if( autoInitSkeleton ) InitSkeletonAnimationExtend(skinType, attackType);
		// 애니메이션이 끝남을 알기위한 이벤트 등록.
		skAnimation.state.End += AnimationEnd;
		// 플레이어 캐릭터 이면 알파값을 빼준다.
		// SkeletoneAnimationExtend 클래스에서는 컬러를 조절 할수 있는 함수가 포함되어 있다.
		if( isPlayer ) skAnimation.ActionFadeTo(3f,0.5f);
		// 몸체 바디의 충돌영역을 2배로 키워준다.
		// 스파인상의 바다 이미지 크기만으로는 영역이 작아 소스상에서 일괄적으로 키워준다.
		skAnimation.bodyBoundingBox.boxCollider2D.size = skAnimation.bodyBoundingBox.boxCollider2D.size * 2f;
	}

	// Update is called once per frame
	void Update () {
		// 키보드 컨트롤.
		if( isPlayer ) ActionProcess ();
	}

	// 키보드 컨트롤.
	void ActionProcess(){
		moveVector = Vector3.zero;
		// 키보드에 의한 움직임.
		if( ! isOneShotAnimationning ) moveVector = GetMoveVectFromInputKey().normalized;
		// 단축키 처리.
		CheckInputKey();
		// 움직임 벡터에 따라 애니메이션 상태를 변경 시켜 준다.
		AutoSetAniState( moveVector.magnitude );

		// Flip
		if( moveVector.x > 0 && mytf.eulerAngles.y != 0 ) 
			mytf.eulerAngles = new Vector3(0,0,0);
		else if( moveVector.x < 0 && mytf.eulerAngles.y != 180 ) 
			mytf.eulerAngles = new Vector3(0,180,0);


		//**start move by translate
		Vector3 myPos = mytf.position;
		myPos += moveVector * (moveSpeed * Time.smoothDeltaTime);

//		Rect rect = KDI_MapTool.i.characterMoveRect;
//		if (myPos.x < rect.xMin || myPos.x > rect.xMax)	myPos.x = mytf.position.x;
//		if (myPos.y < rect.yMin || myPos.y > rect.yMax)	myPos.y = mytf.position.y;

		mytf.position = myPos;
		// **end move by translate */
	}
	// 키보드 컨트롤.
	Vector3 GetMoveVectFromInputKey(){
		return new Vector3( Input.GetAxis ("Horizontal"),Input.GetAxis ("Vertical"),0f);
	}
	// 단축키.
	// 병과 변경, 스킨 변경.
	void CheckInputKey(){
		// 공격 버튼.
		if( Input.GetKeyUp( KeyCode.Space )){
			ActionAttack();
		}
		else if( Input.GetKeyUp( KeyCode.Tab ) ){

		}
		// 병과 및 공격 타입 변경.
		else if( Input.GetKeyUp( KeyCode.Alpha1 ) ){
			InitSkeletonAnimationExtend(SkinType.basic, AttackType.a);
		}else if( Input.GetKeyUp( KeyCode.Alpha2 ) ){
			InitSkeletonAnimationExtend(SkinType.s2, AttackType.b);
		}else if( Input.GetKeyUp( KeyCode.Alpha3 ) ){
			InitSkeletonAnimationExtend(SkinType.s3, AttackType.c);
		}else if( Input.GetKeyUp( KeyCode.Alpha4 ) ){
			InitSkeletonAnimationExtend(SkinType.s4, AttackType.d);
		}else if( Input.GetKeyUp( KeyCode.Alpha5 ) ){
			InitSkeletonAnimationExtend(SkinType.s5, AttackType.e);
		}
	}

	// 스켈레톤 애니메이션 클래스를 초기화 한다.
	public void InitSkeletonAnimationExtend( ){
		InitSkeletonAnimationExtend(skinType, attackType);
	}
	// 스켈레톤 애니메이션 클래스를 초기화 한다.
	// 바운딩 영역을 결정 하는 본과 이미지 네임을 하드코딩으로 자동 처리 해주는 부분.
	public void InitSkeletonAnimationExtend( SkinType sType, AttackType aType){
		// 장수의 경우 스킨 변경.
		if( characterType == CharacterType.General )
		{
			ChangeSkinType( sType );
		}
		// 병사 일경우 공격 타입 변경.
		else if( characterType == CharacterType.Soldier ){
			ChangeAttackType( aType );
		}
		// 몸체 충돌 영역 설정.
		skAnimation.bodyBoundingBox.InitBoundingBox("Head", "Body");

		// 최종 설정후 반드시 애니메이션 클립을 하나 호출 해줘야 한다. 
		SetClip( SpineExtendCharacter.ClipType.idle );
	}

	// 스킨 변경.
	// 장수용 함수로써 무기 본의 이름과 해당 이미지 네임을 가지고 영역을 잡아주는 역활을 한다.
	// 하드 코딩 소스.
	// 원칙 적으로는 BoneBoundingBox의 변수를 조절 하여 잡는 것이 맞으나 소스상에서 처리를 해준다.
	public void ChangeSkinType( SkinType sType ){

		skAnimation.skeleton.SetSkin( sType.ToString() );

		switch ( sType ){
		case SkinType.basic : skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon2","Weapon_01"); break;
		case SkinType.s2 	: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon2","Weapon_02"); break;
		case SkinType.s3 	: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon2","Weapon_03"); break;
		case SkinType.s4 	: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon2","Weapon_04"); break;
		case SkinType.s5 	: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon2","Weapon_05"); break;
		case SkinType.s6 	: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon2","Weapon_06"); break;
		}
	}

	// you must modify to name with SkeletoneAnimationExtend
	public void ChangeAttackType( AttackType aType ){
        Debug.Log(this.gameObject.name);

		attackType = aType;
		SetClip( ClipType.idle );

		switch( aType ){
		case AttackType.a: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon_lance2"	,"Weapon_lance", true); break;
		case AttackType.b: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon_sword2"	,"Weapon_sword", true); break;
		case AttackType.c: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon_Arrow2"	,"Arrow_01", true); break;
		case AttackType.d: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon_stone2"	,"Weapon_stone", true); break;
		case AttackType.e: skAnimation.weaponBoundingBoxs[0].InitBoundingBox( "Weapon_Hammer2"	,"Weapon_Hammer", true); break;
		}
	}
	// 공격 명령.
	void ActionAttack(){
		SetClip(ClipType.atk);

		// 원거리 병과의 경우 총알을 발사 시킨다. 
		if( attackType == AttackType.c )
			bulletMaker.ActionBulletMaking(skAnimation.weaponBoundingBoxs[0]);
		else if( attackType == AttackType.d )
			bulletMaker.ActionBulletMaking(skAnimation.weaponBoundingBoxs[0]);
	}
	// 애니메이션이 1회 끝나면 호출. 
	void AnimationEnd (Spine.AnimationState state, int trackIndex){
		isOneShotAnimationning = false;
	}
	// SetAnimation 간편 함수.
	void SetClip( string name ){
		SetClip( (ClipType) Enum.Parse( typeof(ClipType), name ) );
	}
	// SetAnimation 간편 함수.
	// 현재 1회성 애니 메이션인지. idle과 같은 반복형 애니가 구현중인지 알기 위한 처리 구문.
	void SetClip( ClipType cType){
		currentClipType = cType;

		skAnimation.state.ClearTracks();

		if( currentClipType == ClipType.idle || currentClipType == ClipType.run )
		{
			SetAnimation( cType , true);
		}
		else
		{
			isOneShotAnimationning = true;
			SetAnimation( cType , false);
		}
	}
	// 병과별, 장수별 애니 클립 네이밍이 달라 파싱해주는 함수.
	void SetAnimation( ClipType clipType, bool isLoop ){
		if( characterType == CharacterType.General )
			skAnimation.state.SetAnimation(0,clipType.ToString(),isLoop);
		else 
			skAnimation.state.SetAnimation(0,attackType.ToString() + "_" + clipType.ToString(),isLoop);
	}
	// moveVector의 값에 따라 애니메이션 처리를 어찌 할 것인가 결정 해주는 함수. 
	void AutoSetAniState(float magnitude){
		if( isOneShotAnimationning ) return;

		if( magnitude > 0 && currentClipType != ClipType.run)
		{
			SetClip( ClipType.run );
		}
		else if( magnitude <= 0 && currentClipType != ClipType.idle )
		{
			SetClip( ClipType.idle );
		}
	}
}
