﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpineExtendCharacterCreate : MonoSingleton< SpineExtendCharacterCreate > {

	// character type, character id, skin type, attack type, 
	public int[,] enemyJson = new int[,]{ 
		{ 0,0,0,0 },{ 0,1,1,0 },{ 0,2,2,0 },{ 0,3,3,0 },{ 0,4,4,0 },
		{ 1,1,0,1 },{ 1,2,0,2 },{ 1,3,0,3 },{ 1,4,0,4 },{ 1,5,0,0 },
		{ 1,2,0,2 },{ 1,3,0,3 },{ 1,4,0,4 },{ 1,5,0,0 },{ 1,0,0,0 },
		{ 1,3,0,3 },{ 1,4,0,4 },{ 1,5,0,0 },{ 1,0,0,0 },{ 1,1,0,1 },
	};

	public SpineExtendCharacter characterPrefab;

	public Transform[] createPos;

	public List< SpineExtendCharacter > enemies = new List<SpineExtendCharacter>();

	// Use this for initialization
	void Start () {
		StartCoroutine( CreateProc() );
	}
	
	// Update is called once per frame
	void Update () {
		if(enemies.Count > 0 &&  Input.GetKeyDown(KeyCode.X) ){
			Destroy( enemies[ enemies.Count - 1 ].gameObject );
			enemies.RemoveAt( enemies.Count - 1 );
		}
	}

	void CreateCharacter(){
		// Create Character

		SpineExtendCharacter ch = Instantiate( characterPrefab ) as SpineExtendCharacter;

		// Choice Character Type
		// General
		int len = enemyJson.GetLength(0);

		int characterJsonIdx = Random.Range(0, len );

		ch.characterType 	= (SpineExtendCharacter.CharacterType) 	enemyJson[characterJsonIdx,0];

		if( ch.characterType == SpineExtendCharacter.CharacterType.Soldier )
			ch.skAnimation.SetSkeletonDataAsset( SkeletonDataAssetManager.i.Soldiers[ enemyJson[ characterJsonIdx, 1 ] ] );
		else 
			ch.skAnimation.SetSkeletonDataAsset( SkeletonDataAssetManager.i.Generals[ enemyJson[ characterJsonIdx, 1 ] ] );

		ch.skinType 		= (SpineExtendCharacter.SkinType) 		enemyJson[characterJsonIdx,2];

		ch.attackType 		= (SpineExtendCharacter.AttackType) 	enemyJson[characterJsonIdx,3];

		ch.transform.position = createPos[ Random.Range(0,createPos.Length) ].position;

		ch.transform.parent = transform;

		ch.InitSkeletonAnimationExtend();



		enemies.Add( ch );
	}

	IEnumerator CreateProc(){
		yield return new WaitForSeconds(0f);

		while( true ){
			if( enemies.Count == 0 ) CreateCharacter();
			yield return new WaitForSeconds(1f);
		}
	}


}
