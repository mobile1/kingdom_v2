﻿using UnityEngine;
using System.Collections;

public class SpineExtendCharacterCollider : MonoBehaviour {

	public SpineExtendCharacter character;

	void OnTriggerStay2D (Collider2D coll) {

        Debug.Log(this.transform.parent.parent.name);
		// check weapon
//		if( coll.gameObject.tag != "weapon" ) return;
		// check parent
		if( coll.transform == transform.parent.parent ) return;

		SpineExtendCharacterCreate.i.enemies.Remove( character );
		Destroy( character.gameObject );
	}
}
