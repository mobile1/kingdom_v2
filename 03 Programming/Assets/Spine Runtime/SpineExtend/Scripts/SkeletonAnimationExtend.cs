﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
[AddComponentMenu("Spine/SkeletonAnimationExtend")]
public class SkeletonAnimationExtend : SkeletonAnimation {

	public List< BoneBoundingBox > weaponBoundingBoxs = new List<BoneBoundingBox>();

	public BoneBoundingBox bodyBoundingBox;

	public void SetSkeletonDataAsset( SkeletonDataAsset data){
		this.skeletonDataAsset = data;
		this.initialSkinName = "";
		this.AnimationName = "";
		this.Reset();
	}

	public void CreateWeaponBoundingBox(){
		GameObject go  = new GameObject("Bone");
		go.transform.parent = transform;
		go.transform.localPosition = Vector3.zero;
		weaponBoundingBoxs.Add( go.AddComponent<BoneBoundingBox>() as BoneBoundingBox );

		int idx = weaponBoundingBoxs.Count -1;

		weaponBoundingBoxs[ idx].skAnimation = this;

		string boneName = skeleton.Bones[0].Data.name;
		string spriteName = this.skeletonDataAsset.spriteCollection.spriteDefinitions[0].name;

		weaponBoundingBoxs[ idx].InitBoundingBox( boneName, spriteName );

		weaponBoundingBoxs[ idx].useRealTimeBoundingUpdate = true;
	}

	public void DeleteWeaponBoundingBox(int idx){
		if( weaponBoundingBoxs.Count == 0 ) return;
		if( weaponBoundingBoxs[ idx ] != null ) DestroyImmediate( weaponBoundingBoxs[ idx ].gameObject , true );
		weaponBoundingBoxs.RemoveAt( idx );
	}

	public void CreateBodyBoundingBox(){
		GameObject go = new GameObject("Body");
		go.transform.parent = transform;
		go.transform.localPosition = Vector3.zero;
		bodyBoundingBox = go.AddComponent<BoneBoundingBox>() as BoneBoundingBox;

		bodyBoundingBox.skAnimation = this;

//		string boneName = skeleton.Bones[0].Data.name;
//		string spriteName = this.skeletonDataAsset.spriteCollection.spriteDefinitions[0].name;

		bodyBoundingBox.InitBoundingBox( "Body", "Body" );

		bodyBoundingBox.useRealTimeBoundingUpdate = false;
	}

	public void DeleteBodyBoundingBox(){
		if( bodyBoundingBox != null ) DestroyImmediate( bodyBoundingBox.gameObject , true );
		bodyBoundingBox = null;
	}

	public Color m_color = Color.white;
	public Color color{
		get{return m_color;}
		set{ 
			if( skeleton != null ){
				skeleton.R = m_color.r = value.r;
				skeleton.G = m_color.g = value.g;
				skeleton.B = m_color.b = value.b;
				skeleton.A = m_color.a = value.a;
			}else{
				m_color = value;
			}
		}
	}
	
	public float R{ set{ skeleton.R = value; }}
	public float G{ set{ skeleton.G = value; }}
	public float B{ set{ skeleton.B = value; }}
	public float A{ set{ skeleton.A = value; }}

	public bool isFading;
	// 주어진 시간동안 알파 값이 빠져 버린다.
	public void ActionFadeTo(float time, float alpha){ 
		if( ! isFading ) StartCoroutine (ActionFadeToIE (time, alpha));
	}
	IEnumerator ActionFadeToIE(float time, float alpha){
		isFading = true;
		float timeinc = 0;
		Color tColor = new Color(skeleton.R,skeleton.G,skeleton.B,skeleton.A);
		
		if( alpha > 1) alpha = 1f;
		if( alpha < 0) alpha = 0f;
		
		float a = alpha - tColor.a;
		float checkTime = 0.1f;
		
		while( timeinc < time){
			// 시간 증가량 * 범위 / 전체 시간
			skeleton.A += checkTime * a / time;
			
			timeinc += checkTime;
			
			yield return new WaitForSeconds (checkTime);
		}
		
		skeleton.A = alpha;
		
		isFading = false;
	}
	
	// 주어진 시간동안 컬러 값이 빠져 버린다.
	public void ActionColorTo(float time, Color color){ 
		if( ! isFading ) StartCoroutine (ActionColorToIE (time, color));
	}
	IEnumerator ActionColorToIE(float time, Color color){
		isFading = true;
		
		float timeinc = 0;
		Color tColor = new Color(skeleton.R,skeleton.G,skeleton.B,skeleton.A);
		float r = color.r - tColor.r;
		float g = color.g - tColor.g;
		float b = color.b - tColor.b;
		float a = color.a - tColor.a;
		
		float checkTime = 0.1f;
		
		while( timeinc < time){
			
			skeleton.R += checkTime * r / time;
			skeleton.G += checkTime * g / time;
			skeleton.B += checkTime * b / time;
			skeleton.A += checkTime * a / time;
			
			timeinc += checkTime;
			
			yield return new WaitForSeconds (checkTime);
		}
		
		skeleton.R = color.r;
		skeleton.G = color.g;
		skeleton.B = color.b;
		skeleton.A = color.a;
		
		isFading = false;
	}

}
