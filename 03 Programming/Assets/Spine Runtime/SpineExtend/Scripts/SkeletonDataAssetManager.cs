﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SkeletonDataAssetManager : MonoSingleton<SkeletonDataAssetManager> {

	public List< SkeletonDataAsset> Soldiers = new List<SkeletonDataAsset>();
	public List< SkeletonDataAsset> Generals = new List<SkeletonDataAsset>();

	[System.Serializable]
	public class ObjectFolder{
		[HideInInspector]
		public string name;
		public Object folder;
		public ObjectInfo[] objectInfos;
	}
	
	[System.Serializable]	
	public class ObjectInfo{		
		[HideInInspector]
		public string name;
		public GameObject prefab;
		
		//Construct
		public ObjectInfo(GameObject go){
			prefab = go;
			name = prefab.name;			
		}
	}
	
	public Dictionary<string, SkeletonDataAsset> collection = new Dictionary<string, SkeletonDataAsset>();
	
	public ObjectFolder[] folders;
	


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CreateObject(string key){
//		collection[key].AddCreateObject(true,Vector3.zero);
	}

	public void CreateCollection(){
		
		//			collections_debug.Clear();
		
//		collections.Clear();
		
//		for(int fid = 0; fid < folders.Length; fid++){
//			foreach ( GPPrefabInfo info in folders[fid].prefabInfos ){
//				GPCollection collection = new GPCollection( info );	
//				
//				//				collections_debug.Add( collection );
//				collections.Add(  info.prefab.name, collection );
//			}
//		}
	}
}
