﻿using UnityEngine;
using System.Collections;
using Spine;

//[ExecuteInEditMode]
public class BoneBoundingBox : MonoBehaviour {
	public SkeletonAnimationExtend skAnimation;
	public int BoneIdx{get;set;}

	public Spine.Bone bone;
	public string boneName,spriteName;

	public float imageBaseRotation = 270f;

	public BoxCollider2D boxCollider2D;

	public bool useRealTimeBoundingUpdate = true;

	// Use this for initialization
	void Start () {
//		print(gameObject.name + " "  + boneName + " " + spriteName);
		if( bone == null ) 	InitBoundingBox ( boneName, spriteName );
	}
	
	// Update is called once per frame
	void Update () {
		if (bone != null && useRealTimeBoundingUpdate )	SyncherProc ();
	}

	public void CreateBoxCollider(){

		boxCollider2D = gameObject.AddComponent<BoxCollider2D>() as BoxCollider2D;
		gameObject.AddComponent< Rigidbody2D>();

		// Setting RigidBody
		rigidbody2D.gravityScale = 0;
		rigidbody2D.fixedAngle = true;
		boxCollider2D.isTrigger = true;
	}

	public void ReBounds(string name){

		Bounds bounds = skAnimation.skeletonDataAsset.spriteCollection.GetSpriteDefinition( name ).GetBounds();

		if( bounds == null ) print( "Error : Not Found sprite name " + name);

		if( boxCollider2D == null ) CreateBoxCollider();

		boxCollider2D.size = bounds.size;
		boxCollider2D.center = bounds.center;
	}

    /////////////////////  2014-08-03 15:31  Jaeun Yun 
    // 무기일 경우 오브젝트 이름을 AttackBox로 만든다.
    //public void InitBoundingBox(string _boneName, string _spriteName)
    public void InitBoundingBox(string _boneName, string _spriteName, bool bWeapon = false)
    ///////////////////////////////////////////////////////////////////////////
    {
//		print(gameObject.name + " "  + _boneName + " " + _spriteName);

        // yun
        //  게임 오브젝트 이름은 통일시켜서 충돌 체크를 하도록 한다.
		//  AttackBox로 통일하도록 한다.
        //  gameObject.name = boneName = _boneName;
        if(bWeapon)
            gameObject.name = "AttackBox";
        
        boneName = _boneName;
		spriteName = _spriteName;


		if (skAnimation != null)
		{
			bone = skAnimation.skeleton.FindBone( boneName );

			if( bone == null ) print ("Error : Not found boneName " + boneName );

			ReBounds( spriteName );

			SyncherProc();
		}
		else{
			print ( "Error Skeletone Animamtion ");
		}
	}

	void SyncherProc(){

		Transform parentTF = skAnimation.transform;
		Vector3 basePos = parentTF.position;

		// Check Flip by Rotation
		// if( ! skAnimation.IsFlip )
		if( parentTF.eulerAngles.y == 0 )
		{
			transform.localPosition = new Vector3 (bone.WorldX,bone.WorldY, transform.localPosition.z);
//			transform.localRotation = Quaternion.Euler (0, parentTF.eulerAngles.y, bone.WorldRotation + imageBaseRotation);
			transform.localRotation = Quaternion.Euler (0, 0, bone.WorldRotation + imageBaseRotation);
			transform.localScale	= new Vector3 (bone.WorldScaleX, bone.WorldScaleX, transform.localScale.z );
		}
		else
		{
			transform.position = basePos + new Vector3 (- bone.WorldX, bone.WorldY, transform.localPosition.z);
//			transform.localRotation = Quaternion.Euler (0, parentTF.eulerAngles.y, bone.WorldRotation + imageBaseRotation);
			transform.rotation = Quaternion.Euler (0, 180, bone.WorldRotation + imageBaseRotation);
			transform.localScale	= new Vector3 (bone.WorldScaleX, bone.WorldScaleX, transform.localScale.z );
		}

	}

	public void SetSkeletoneAnimationExtend( SkeletonAnimationExtend ske, string _boneName, string _spriteName ){
		skAnimation = ske;

		print (_boneName + "  " + _spriteName );
		CreateBoxCollider();

		InitBoundingBox ( _boneName, _spriteName );

		SyncherProc();
	}


}
