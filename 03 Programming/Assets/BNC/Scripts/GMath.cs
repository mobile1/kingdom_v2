using UnityEngine;
using System.Collections;

public static class GMath{

	public static float GetAngleOfLine(Vector2 p1, Vector2 p2){
		float rtVal = Mathf.Atan2((p2.y - p1.y),(p2.x - p1.x)) *  180f / Mathf.PI;
		rtVal = (rtVal < 0 ? (360f + rtVal) : rtVal);
		rtVal = (rtVal > 360f ? (rtVal - 360f) : rtVal);
		return rtVal;
	}
	
	public static Vector3 GetDirectByArcAngle(Vector3 direct, float angle){
		Vector2 pPoint = new Vector2(0, 0);
		float fAngleTemp = angle * Mathf.PI/180;
		
		//Only 2D Process
		pPoint.x = (direct.x * Mathf.Cos(fAngleTemp)) - (direct.y * Mathf.Sin(fAngleTemp));
		pPoint.y = (direct.x * Mathf.Sin(fAngleTemp)) + (direct.y * Mathf.Cos(fAngleTemp));
		
		return new Vector3(pPoint.x,pPoint.y,0);
	}
	
	public static void ChanagePosByDirectByArcAngle(ref Vector3 posA, ref Vector3 posB,Vector3 pCenter, float angle){
		
		Vector3 dirCA1 = GetDirectByArcAngle((pCenter - posA).normalized,angle);
		Vector3 dirCB1 = GetDirectByArcAngle((pCenter - posB).normalized,angle);
		
		posA = pCenter + dirCA1 * Vector3.Distance(pCenter, posA);
		posB = pCenter + dirCB1 * Vector3.Distance(pCenter, posB);
	}
		
	
	public static Vector3 GetPointByArcAngle(Vector3 basePos, Vector3 direct, float angle){
		return basePos + GetDirectByArcAngle(direct,angle);
	}
	
	public static Vector3 GetPointPosToPos(Vector3 pos1, Vector3 pos2, float percentage){
		Vector3 direct = pos2 - pos1;
		float distance = Vector3.Distance(pos2,pos1) * 0.9f;
		return pos1 + (direct.normalized * distance);	
	}
	
	/// This is based off an explanation and expanded math presented by Paul Bourke:
	/// It takes two lines as inputs and returns true if they intersect, false if they don't.
	/// If they do, ptIntersection returns the point where the two lines intersect.  
	public static bool DoLinesIntersect(Vector2 L11,Vector2 L12,Vector2 L21,Vector2 L22,ref Vector2 intersection){
		// Denominator for ua and ub are the same, so store this calculation
		float d = (L22.y - L21.y) * (L12.x - L11.x) - (L22.x - L21.x) * (L12.y - L11.y);
		//n_a and n_b are calculated as seperate values for readability
		float n_a = (L22.x - L21.x) * (L11.y - L21.y) - (L22.y - L21.y) * (L11.x - L21.x);
		float n_b = (L12.x - L11.x) * (L11.y - L21.y) - (L12.y - L11.y) * (L11.x - L21.x);
		
		// Make sure there is not a division by zero - this also indicates that
		// the lines are parallel.  
		// If n_a and n_b were both equal to zero the lines would be on top of each 
		// other (coincidental).  This check is not done because it is not 
		// necessary for this implementation (the parallel check accounts for this).
		if (d == 0)	return false;
		
		// Calculate the intermediate fractional point that the lines potentially intersect.
		float ua = n_a / d;
		float ub = n_b / d;
		
		// The fractional point will be between 0 and 1 inclusive if the lines
		// intersect.  If the fractional calculation is larger than 1 or smaller
		// than 0 the lines would need to be longer to intersect.
		if (ua >= 0d && ua <= 1d && ub >= 0d && ub <= 1d){
			intersection.x = L11.x + (ua * (L12.x - L11.x));
			intersection.y = L11.y + (ua * (L12.y - L11.y));
			return true;
		}
		return false;
	}
	public static Vector3 Get3DPointFrom2DPoint(Camera camera,Vector2 pos, float depth){	
		Ray ray = camera.ScreenPointToRay(pos);
		return ray.GetPoint(depth);
	}
	public static bool Convert3DPointFrom2DPoint(Camera camera,ref Vector3 pos3D,Vector2 pos2D, GameObject target){	
		RaycastHit hit = new RaycastHit();
		Ray ray = camera.ScreenPointToRay(pos2D);
		if (Physics.Raycast (ray.origin,ray.direction,out hit, 100)) {
			if(hit.collider.gameObject == target){
				pos3D = hit.point;
				return true;
			}
		}
		return false;
	}
	
	public static Vector3 GetCenter3DPointFromLine(Vector3 posA, Vector3 posB){
		return new Vector3((posB.x + posA.x)/2f,(posB.y + posA.y)/2f,(posB.z + posA.z)/2f);	
	}
	public static Vector2 GetCenter2DPointFromLine(Vector3 posA, Vector3 posB){
		return new Vector2((posB.x + posA.x)/2f,(posB.y + posA.y)/2f);	
	}
	
	public static Vector3 InversePoint(Vector3 basePos, Vector3 targetPos){
		return 2f*basePos - targetPos;	
	}
	//반올림, digits값이 0이거나 잘못 되었을때 -1 반환.
	public static float GetBan( float val , float digits)
	{
		if(digits == 0.0f)
			return -1;
		
		float changeToDecimal = digits;
		if(digits > 1)
		{
			while(true)
			{
				if(changeToDecimal < 10)
				{
					break;
				}
				else
				{
					changeToDecimal /= 10;
				}
			}
		}
		else if(digits > 0 && digits < 1)
		{
			while(true)
			{
				changeToDecimal *= 10;
				
				if(changeToDecimal % 10 == 0)
					break;
			}
		}
		else if(digits < 0)
		{
			return -1;
		}
		
		digits /= changeToDecimal;
		
		if(digits > 1)
		{
			digits *= 10;
		}
		
		float temp = val / digits;
		bool isEvenNum;
		bool isUp;
		
		int integer = (int)temp;
		float dec = temp - integer;
		
		dec *= 10;

		isUp = dec >= 5 ? true : false;
		// 0 is even , or not odd
		isEvenNum = temp % 2 == 0 ? true : false;

		temp = Mathf.Round(temp);
		
		if(isEvenNum && isUp)
		{
			temp += 1;
		}
		return temp * digits;
	}

	public static Vector2 RandomVector2(float xRange, float yRange)
	{
		Vector2 vec = new Vector2(Random.Range(-1.0f, 1.0f) * xRange, Random.Range(-1.0f, 1.0f) * yRange);
		return vec;
	}
	
	public static Vector3 RandomVector3(float xRange, float yRange,float zRange)
	{
		Vector3 vec = new Vector3(Random.Range(-1.0f, 1.0f) * xRange, Random.Range(-1.0f, 1.0f) * yRange, Random.Range(-1.0f, 1.0f) * zRange);
		return vec;
	}

	public static Vector3 ChageVector3(Vector3 temp, string target, float value){
//		Vector3 temp = vector;
		switch(target){
		case "x": temp.x = value;	break;
		case "y": temp.y = value;	break;
		case "z": temp.z = value;	break;
		}
//		vector = temp;
		return temp;
	}
}
