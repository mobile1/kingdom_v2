﻿using UnityEngine;
using System.Collections;

public class GDeviceInfo : MonoBehaviour {

	float timeA; 
	
	int fps;
	
	int lastFPS;
	
	UILabel labelFPS;
		
	// Use this for initialization
	
	void Start () {

		labelFPS = GetComponent<UILabel> ();

		timeA = Time.timeSinceLevelLoad;
		
		DontDestroyOnLoad (this);
	}

	// Update is called once per frame
	void Update () {	
#if UNITY_ANDROID
		if (Input.GetKeyDown(KeyCode.Escape)) { Application.Quit(); }
#endif
		CheckFPS ();
	}

	void CheckFPS(){
		if(Time.timeSinceLevelLoad  - timeA <= 1)
		{
			fps++;	
		}
		else	
		{
			lastFPS = fps + 1;
			
			timeA = Time.timeSinceLevelLoad;
			
			fps = 0;	
		}
		
		labelFPS.text = "FPS : " + lastFPS.ToString("f2");	
	}
}
