using UnityEngine;
using System.Collections;

public class UIGridExtendFunction : MonoBehaviour {
    public enum FunctionType{
        Add,Delete,DeleteFirst,DeleteLast,DeleteAll,Insert,SetCursor,MoveFirst,MoveLast,MoveNext,MovePre,    
    }
    public FunctionType functionType;
    public int functionIndex;
    
    public UIGridExtend itemList;

    void OnClick(){
        switch(functionType){
        case FunctionType.Add:          itemList.Add();               break;   
        case FunctionType.Delete:           itemList.RemoveAt( functionIndex );           break;   
        case FunctionType.DeleteAll:       itemList.Clear();      break;   
        case FunctionType.DeleteFirst:    itemList.RemoveFirst();    break;   
        case FunctionType.DeleteLast:    itemList.RemoveLast();     break;   
        case FunctionType.Insert:            itemList.Insert(functionIndex);             break;   
        case FunctionType.SetCursor:           itemList.SetCursor(functionIndex);              break;   
        case FunctionType.MoveFirst:    itemList.MoveFirst();      break;   
        case FunctionType.MoveLast:      itemList.MoveLast();         break;
        case FunctionType.MoveNext:    itemList.MoveNext();      break;   
        case FunctionType.MovePre:      itemList.MovePre();         break;    
        }
    }
    
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
