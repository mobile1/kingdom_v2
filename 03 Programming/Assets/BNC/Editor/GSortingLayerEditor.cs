

using System;
using System.Reflection;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GSortingLayer))]
public class SortingLayerExposedEditor : Editor {
private string[] _sortingLayerNames;

void OnEnable()  {
    var internalEditorUtilityType = Type.GetType("UnityEditorInternal.InternalEditorUtility, UnityEditor");
    var sortingLayersProperty = internalEditorUtilityType.GetProperty("sortingLayerNames", BindingFlags.Static | BindingFlags.NonPublic);
    _sortingLayerNames = sortingLayersProperty.GetValue(null, new object[0]) as string[];
}

public override void OnInspectorGUI() {
    // Get the renderer from the target object
		var renderer = (target as GSortingLayer).gameObject.renderer;

    // If there is no renderer, we can't do anything
    if (!renderer) {
        return;
    }

    // If we have the sorting layers array, we can make a nice dropdown. For stability's sake, if the array is null
    // we just use our old logic. This makes sure the script works in some fashion even if Unity changes the name of
    // that internal field we reflected.
    if (_sortingLayerNames != null) {
        // Expose drop down for sorting layer
        int layerIndex = Array.IndexOf(_sortingLayerNames, renderer.sortingLayerName);
        int newLayerIndex = EditorGUILayout.Popup("Sorting Layer", layerIndex, _sortingLayerNames);
        if (newLayerIndex != layerIndex) {
            Undo.RecordObject(renderer, "Edit Sorting Layer");
            renderer.sortingLayerName = _sortingLayerNames[newLayerIndex];
            EditorUtility.SetDirty(renderer);
        }
    }
    else {
        // Expose the sorting layer name
        string newSortingLayerName = EditorGUILayout.TextField("Sorting Layer Name", renderer.sortingLayerName);
        if (newSortingLayerName != renderer.sortingLayerName) {
            Undo.RecordObject(renderer, "Edit Sorting Layer Name");
            renderer.sortingLayerName = newSortingLayerName;
            EditorUtility.SetDirty(renderer);
        }

        // Expose the sorting layer ID
        int newSortingLayerId = EditorGUILayout.IntField("Sorting Layer ID", renderer.sortingLayerID);
        if (newSortingLayerId != renderer.sortingLayerID) {
            Undo.RecordObject(renderer, "Edit Sorting Layer ID");
            renderer.sortingLayerID = newSortingLayerId;
            EditorUtility.SetDirty(renderer);
        }
    }

    // Expose the manual sorting order
    int newSortingLayerOrder = EditorGUILayout.IntField("Sorting Layer Order", renderer.sortingOrder);
    if (newSortingLayerOrder != renderer.sortingOrder) {
        Undo.RecordObject(renderer, "Edit Sorting Order");
        renderer.sortingOrder = newSortingLayerOrder;
        EditorUtility.SetDirty(renderer);
    }
}
}
