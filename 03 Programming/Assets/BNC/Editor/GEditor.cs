﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;


using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using System.IO;
//using GMT;

using System.Text;
using System;

public class GEditor: Editor {
	#if UNITY_EDITOR
	protected GUIStyle style = new GUIStyle();
	protected bool showDefaultVariablies;
	protected string showDefaultVariabliesKey;
	
	protected virtual void OnEnable(){
		//i like bold handle labels since I'm getting old:
		style.fontStyle = FontStyle.Bold;
		style.normal.textColor = Color.white;
	}

	protected virtual void OnDisable(){

	}

	public override void OnInspectorGUI(){	
		showDefaultVariablies = Boolean.Parse( PlayerPrefs.GetString(showDefaultVariabliesKey,"false") );
		bool temp = EditorGUILayout.Foldout(showDefaultVariablies, "Default Variablies Setting");
		if( temp != showDefaultVariablies){
			showDefaultVariablies = temp;
			PlayerPrefs.SetString(showDefaultVariabliesKey,showDefaultVariablies.ToString());
		}

		if(showDefaultVariablies ) DrawDefaultInspector();

	}


	protected bool MyButton(string title){
		int addCount = title.Length;
		
		if( addCount < 10 ) for( int i = 0; i < (10 - addCount)/2 + 1; i++)	title = "_" + title + "_";
		
		return GUILayout.Button(title.ToString());
	}
	
	protected static T[] GetAtFilePath<T> (string path) {
		
		ArrayList al = new ArrayList();
		
		string [] fileEntries = Directory.GetFiles(path);
		
		foreach(string fileName in fileEntries){
			int index = fileName.LastIndexOf("/");
			
			string localPath = path;
			
			if (index > 0) localPath += fileName.Substring(index);
			
			UnityEngine.Object t = Resources.LoadAssetAtPath(localPath, typeof(T));
			if(t != null) al.Add(t);
		}
		
		T[] result = new T[al.Count];
		
		for(int i=0;i<al.Count;i++) result[i] = (T)al[i];
		
		return result;
	}
	
	protected static T[] GetAtDirectoryPath<T> (string path) {
		
		ArrayList al = new ArrayList();
		
		string [] fileEntries = Directory.GetDirectories(path);
		
		foreach(string fileName in fileEntries){
			int index = fileName.LastIndexOf("/");
			
			string localPath = path;
			
			if (index > 0) localPath += fileName.Substring(index);
			
			UnityEngine.Object t = Resources.LoadAssetAtPath(localPath, typeof(T));
			if(t != null) al.Add(t);
		}
		
		T[] result = new T[al.Count];
		
		for(int i=0;i<al.Count;i++) result[i] = (T)al[i];
		
		return result;
	}

	protected Component CopyComponent(Component original, GameObject destination)
	{
		System.Type type = original.GetType();
		Component copy = destination.AddComponent(type);
		// Copied fields can be restricted with BindingFlags
		System.Reflection.FieldInfo[] fields = type.GetFields(); 
		foreach (System.Reflection.FieldInfo field in fields)
		{
			field.SetValue(copy, field.GetValue(original));
		}
		return copy;
	}
	protected void CopyComponentValue(Component original, Component destination)
	{
		System.Type type = original.GetType();
		// Copied fields can be restricted with BindingFlags
		System.Reflection.FieldInfo[] fields = type.GetFields(); 
		foreach (System.Reflection.FieldInfo field in fields)
		{
			Debug.Log(field.GetValue(original));
			field.SetValue(destination, field.GetValue(original));
		}
	}

	public static object DeepCopy(object obj) {
		if(obj==null)
			return null;
		if (obj is UnityEngine.Object)
		{
			return obj;
		}
		Type type=obj.GetType();
		if(type.IsValueType || type==typeof(string)) {
			return obj;
		}
		else if(type.IsArray) {
			Type elementType=Type.GetType(
				type.FullName.Replace("[]",string.Empty));
			var array=obj as Array;
			Array copied=Array.CreateInstance(elementType,array.Length);
			for(int i=0; i<array.Length; i++) {
				copied.SetValue(DeepCopy(array.GetValue(i)),i);
			}
			return Convert.ChangeType(copied,obj.GetType());
		}
		else if(type.IsClass) {
			object toret=Activator.CreateInstance(obj.GetType());
			FieldInfo[] fields=type.GetFields(BindingFlags.Public| 
			                                  /*BindingFlags.NonPublic|*/BindingFlags.Instance);
			foreach(FieldInfo field in fields) {
				object fieldValue=field.GetValue(obj);
				if(fieldValue==null)
					continue;
				field.SetValue(toret,DeepCopy(fieldValue));
			}
			return toret;
		}
		else
			throw new ArgumentException("Unknown type");
	}
	#endif
}
