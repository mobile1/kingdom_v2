using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace GPool{
	//GPool ==> GPPrefabFolders ==> GPPrefabs ==> createObjects
	public class MGSound : MonoSingleton<MGSound> {
		
		//##################################################################################
		// ObjectCollection 
		//
		//
		//##################################################################################
		[System.Serializable]	
		public class GPCollection{
			public List<AudioSource> objects = new List<AudioSource>();
			public GPool.GPPrefabInfo info;
//			int createID = 0;
			public float passTime;
		
			public bool canPlay = true;
				
			public GPCollection(GPPrefabInfo _info){
				info = _info;

				info.defaultCreateCount = Mathf.Max( PlayerPrefs.GetInt(info.name,0), info.defaultCreateCount);

				for ( int i = 0; i < info.defaultCreateCount ; i++){
					AddCreateObject( false , Vector3.zero	 );
				}
			}
			
			
			public AudioSource AddCreateObject ( bool isActive, Vector3 pos){
				GameObject go = Instantiate( info.prefab ) as GameObject; //cerate
				AudioSource audio = go.GetComponent< AudioSource >();
				
				go.name = info.prefab.name;
				go.transform.parent = MGSound.i.transform;
				go.transform.position = pos;													//postioning
				go.gameObject.SetActive( isActive );		
				objects.Add ( audio ); //add
				
				return audio;  //return
			}		
			
			public void Play( Vector3 pos){
				if ( ! canPlay ){ 
					return;	
				}
				
				//미 사용중인 객체를 찾아 활성화 시키고 위치를 조정후 재생 한다.
				for ( int i = 0; i < objects.Count ; i++ ){
					if (! objects[i].isPlaying ){ 	//search
						objects[ i ].gameObject.SetActive(true); 			//active
						objects[ i ].transform.position = pos; 					//positioning
						objects[ i ].Play();													//play
						
						return;
					}
				}
				
				//전부 사용중이어서 위 구문을 지나 친경우 새로 생성 시키고 리스트에 추가 한다.
				PlayerPrefs.SetInt(info.name,objects.Count + 1);
				AddCreateObject(true, pos).Play();
							
			}	
		}
		public GPool.PoolFolder[] folders;
		
		public Dictionary<string, GPCollection> collections = new Dictionary<string, GPCollection>();
	
		bool isInit = false;

		// test
		public void CreateObject(string key){
			PlayerPrefs.SetInt(collections[key].info.name,collections[key].objects.Count + 1);
			collections[key].AddCreateObject(true,Vector3.zero);
		}

		// Use this for initialization
		void Awake () {				
			CreateCollection();
			isInit = true;
		}
				
		public void RenamePrefabInfos(){
			for(int fid = 0; fid < folders.Length; fid++){
				for(int i = 0; i < folders[fid].prefabInfos.Length; i++){
					folders[fid].prefabInfos[i].name = folders[fid].prefabInfos[i].prefab.name + " (" + folders[fid].prefabInfos[i].defaultCreateCount + ")";
				}
			}
		}
		
		/// <summary>
		/// Renames this instance.
		/// 프리펩의 이름에 다가 현재 생성한 오브젝트의 갯수를 표기한다.
		/// </summary>
		public void LoadObjectCount(){
			for(int fid = 0; fid < folders.Length; fid++){
				for(int i = 0; i < folders[fid].prefabInfos.Length; i++){				
					folders[fid].prefabInfos[i].defaultCreateCount = PlayerPrefs.GetInt(folders[fid].prefabInfos[i].name,1);
				}
			}
		}
		
		public void SaveObjectCount(){
			for(int fid = 0; fid < folders.Length; fid++){
				for(int i = 0; i < folders[fid].prefabInfos.Length; i++){				
					PlayerPrefs.SetInt(folders[fid].prefabInfos[i].name, folders[fid].prefabInfos[i].defaultCreateCount);
				}
			}
		}
		
		public void CreateCollection(){
			
//			collections_debug.Clear();
			
			collections.Clear();
			
			for(int fid = 0; fid < folders.Length; fid++){
				foreach ( GPPrefabInfo info in folders[fid].prefabInfos ){
					GPCollection collection = new GPCollection( info );	
					
	//				collections_debug.Add( collection );
					collections.Add( info.prefab.name, collection );
				}
			}
		}
	
		//외부 클래스에서 본 함수를 통해 미리 생성된 객체를 가져다가 사용한다.
		public void PlayOneShot(string key ) {	
			if ( ! isInit ) return;
			if ( ! collections.ContainsKey( key ) ){
				print ( "Error : Pool Object Name : " + key );	
				return;
			}
			if ( ! collections[ key ].canPlay ) return;

//			Debug.Log( key );
			collections[ key ].Play( Vector3.zero );
			StartCoroutine( PassTimer(collections[key]));
			
		}
		
		IEnumerator PassTimer( GPCollection oc ){
			oc.canPlay = false;
			yield return new WaitForSeconds( oc.passTime);	
			oc.canPlay = true;
		}	
	}//End Class
}
