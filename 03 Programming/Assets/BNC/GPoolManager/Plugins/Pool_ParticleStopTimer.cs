﻿using UnityEngine;
using System.Collections;

public class Pool_ParticleStopTimer : MonoBehaviour {
	
	ParticleSystem particle;
	public float stopTime = 1f;
	bool isParticleSystem = false;
	
	void Awake(){
		particle = GetComponent<ParticleSystem>();
		if( particle ) isParticleSystem = true;
		else isParticleSystem = false;
	}
	
	void OnEnable(){
		StartCoroutine ( StopTimer() );	
	}
	
	IEnumerator StopTimer(){
		yield return new WaitForSeconds( stopTime );
		if( isParticleSystem ) particle.Stop();
		gameObject.SetActive(false);
	}
	
}
