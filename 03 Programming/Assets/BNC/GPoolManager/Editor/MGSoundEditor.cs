﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.IO;
using GPool;

[CustomEditor (typeof(MGSound))]
	
public class MGSoundEditor : Editor {
	MGSound pool;
	GUIStyle style = new GUIStyle();
	GPool.PoolFolder[] folders;
	
	void OnEnable(){
		//i like bold handle labels since I'm getting old:
		style.fontStyle = FontStyle.Bold;	
		style.normal.textColor = Color.white;
		pool = (MGSound)target;	
		folders = pool.folders;
	}
	public Object testKeyObject;
	public string testKey;
	
	public override void OnInspectorGUI(){		
		
		if( GUILayout.Button("Reset") ){
			ReloadObject();
			pool.SaveObjectCount();
		}
		
		if( Application.isPlaying ){
			GUILayout.BeginHorizontal();
			{
				testKeyObject = EditorGUILayout.ObjectField("test Object ", testKeyObject,typeof(Object),true);
				if( testKeyObject ) 
				{
					testKey = testKeyObject.name;
					if( GUILayout.Button("Create") ){
						pool.CreateObject(testKey);
					}
				}
			}
			GUILayout.EndHorizontal();
		}
		
		GUILayout.BeginHorizontal();
		{
			if( GUILayout.Button("Load Count") ){
				pool.LoadObjectCount();
			}
			if( GUILayout.Button("Save Count") ){
				pool.SaveObjectCount();
			}
		}
		GUILayout.EndHorizontal();
		
		DrawDefaultInspector();
	}
	
	public void ReloadObject(){
			
		for(int fid = 0; fid < folders.Length; fid++){
			
			string path = AssetDatabase.GetAssetPath( folders[fid].folder );
			
			GameObject[] getObjects = GetAtPath<  GameObject >( path );
			
			folders[ fid ].prefabInfos = new GPPrefabInfo[ getObjects.Length ];
			
			folders[fid].name = path.Replace("Assets/","") + " " + getObjects.Length;
			
			// 해당 경로에 존재 하는 오브젝트를 읽어 들인다.
			for( int oid = 0; oid < getObjects.Length ; oid++){
				
				folders[ fid ].prefabInfos[oid] = new GPPrefabInfo( getObjects[ oid ] );
				
				folders[ fid ].prefabInfos[oid].name = getObjects[ oid ].name;			
			}
		}	
	}
	
	
	public static T[] GetAtPath<T> (string path) {

	        	ArrayList al = new ArrayList();
	
		string [] fileEntries = Directory.GetFiles(path);
		
		foreach(string fileName in fileEntries)
		{
			int index = fileName.LastIndexOf("/");
			
			string localPath = path;
				
			if (index > 0)
				localPath += fileName.Substring(index);
				
			Object t = Resources.LoadAssetAtPath(localPath, typeof(T));
			if(t != null)
				al.Add(t);
		}
		
		T[] result = new T[al.Count];
		
		for(int i=0;i<al.Count;i++)
			result[i] = (T)al[i];
		
		return result;
	}
}
