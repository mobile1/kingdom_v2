﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

public class KDData_AccountInfo : MonoSingleton<KDData_AccountInfo> {
	
	public KDData_Account account;

	[HideInInspector] public bool bOnBgSnd;
	[HideInInspector] public bool bOnEffSnd;
	[HideInInspector] public bool bOnPush;

	private int _level;
	public int level {
		get{
			return _level;
		}
		set{
			_level = SetLevel((int)value);
		}
	}

	public override void Init()
	{
		TextAsset textAsset = (TextAsset)Resources.Load ("Account", typeof(TextAsset));
		try
		{
			account = JsonConvert.DeserializeObject<KDData_Account>(textAsset.text, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
			level = account.exp;
		}
		catch(Exception ex)
		{
			Debug.Log(ex.Message);
			throw;
		}

		bOnBgSnd = true;
		bOnEffSnd = true;
		bOnPush = true;
	}

	int SetLevel(int exp)
	{
		int len = KDDefine.exp.Length;
		int arrIndex = 0;
		for(int i = 0; i < len; ++i)
		{
			if(exp < KDDefine.exp[i])
			{
				arrIndex = i;
				break;
			}
		}
		return arrIndex <= 1 ? 1 : arrIndex;
	}
}
