﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;
using Newtonsoft.Json;

public class KDData_ShopInfo : MonoSingleton<KDData_ShopInfo> {
	public KDData_Shop shopData;

	public override void Init()
	{
		TextAsset textAsset = (TextAsset)Resources.Load ("shopData", typeof(TextAsset));
		try
		{
			shopData = JsonConvert.DeserializeObject<KDData_Shop>(textAsset.text, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
		}
		catch(Exception ex)
		{
			Debug.Log(ex.Message);
			throw;
		}
	}
}
