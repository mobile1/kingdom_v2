﻿using UnityEngine;
using System.Collections.Generic;

public class KDData_Account
{
	public int exp {get;set;}
	public int ticket {get;set;}
	public int coin {get;set;}
	public int cash {get;set;}
	public int atk {get;set;}
	public int def {get;set;}
	public int hp {get;set;}
	public int mp {get;set;}
	public int move {get;set;}
	public int speed {get;set;}
	public int critical {get;set;}
	public int dex {get;set;}
	public int point {get;set;}
	public KDData_StatData StatPoint{get;set;}
	public int curCharacter{get;set;}
	public int curStage{get;set;}
	public List<KDData_Child2> listStage {get;set;}
	//public Dictionary<int, List<KDData_Child> > dicStage{get;set;}
}

public struct KDData_StatData
{
	public int statATK{get;set;}
	public int statMOVE{get;set;}
	public int statDEX{get;set;}
	public int statDEF{get;set;}
}

public class KDData_Child2
{
	public int index{get;set;}
	public List<KDData_Child> detailState {get;set;} 
}
public class KDData_Child
{
	public int star {get;set;}
}
