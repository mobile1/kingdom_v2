﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

public class KDData_CharacterInfo : MonoSingleton<KDData_CharacterInfo> {
	
	public List<KDData_Character> characterList;

	public override void Init()
	{
		TextAsset textAsset = (TextAsset)Resources.Load ("CharInfo", typeof(TextAsset));
		try
		{
			characterList = JsonConvert.DeserializeObject<List<KDData_Character>>(textAsset.text, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
		}
		catch(Exception ex)
		{
			Debug.Log(ex.Message);
			throw;
		}
	}
}
