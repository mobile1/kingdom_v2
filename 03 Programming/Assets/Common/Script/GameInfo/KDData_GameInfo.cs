﻿using UnityEngine;
using System.Collections;

public class KDData_GameInfo : MonoSingleton<KDData_GameInfo>
{

    public struct sStage
    {
        public int level;
        public int detailLevel;
    };

    [HideInInspector]
    static int charIndex;
    static sStage stage;

    public void SetGameData(int _charIndex, int _level, int _detailLevel)
    {
        charIndex = _charIndex;
        stage.level = _level;
        stage.detailLevel = _detailLevel;
    }

    public override void Init()
    {
        DontDestroyOnLoad(this);
    }

    public int GetCharterIndex() { return charIndex; }
    public sStage GetStageInfo() { return stage; }
}