﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using Newtonsoft.Json;

public class KDData_StageInfo : MonoSingleton<KDData_StageInfo> {

	public List<KDData_Stage> stageList;// = new List<KDData_Stage>();

	void Awake()
	{
		TextAsset textAsset = (TextAsset)Resources.Load ("StageInfo", typeof(TextAsset));

		try{
			stageList = JsonConvert.DeserializeObject<List<KDData_Stage>>(textAsset.text, new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All });
		}
		catch(Exception ex)
		{
			Debug.Log(ex.Message);
			throw;
		}
	}
}
