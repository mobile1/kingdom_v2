﻿public class KDDefine
{
	public static int[] exp = {-1, 100, 200, 300, 400, 500, 600, 700, 800, 900, 1000, 1100, 1200, 1300, 1400, 
		1500, 1600, 1700, 1800, 1900, 2000, 99999};

	public enum eSTATEPOINT{POWER, SPEED, DEX, DEF};
	public enum ePOPUPTYPE {NONE, SKININFO};
	public enum eBGTYPE{BASE, ENCHANT};

	public static int[] InvenItemRowPos = {-190,  - 95,  1, 96, 191};
}
/*	public enum EXP_MAX{
		LEVEL1_MAX = 100, 
		LEVEL2_MAX = 200, 
		LEVEL3_MAX = 300, 
		LEVEL4_MAX = 400, 
		LEVEL5_MAX = 500, 
		LEVEL6_MAX = 600, 
		LEVEL7_MAX = 700,
		LEVEL8_MAX = 800,
		LEVEL9_MAX = 900,
		LEVEL10_MAX = 1000,
		LEVEL11_MAX = 1100,
		LEVEL12_MAX = 1200,
		LEVEL13_MAX = 1300,
		LEVEL14_MAX = 1400,
		LEVEL15_MAX = 1500,
		LEVEL16_MAX = 1600,
		LEVEL17_MAX = 1700,
		LEVEL18_MAX = 1800,
		LEVEL19_MAX = 1900,
		LEVEL20_MAX = 2000,
		LEVEL_MAX
	}*/