﻿using UnityEngine;
using System.Collections;

public static class KDData_LData {

	//private static bool setting_bg;
	//private static bool setting_Eff;
	//private static bool settin

	public static void SaveSetting(bool bBg, bool bEff, bool bPush)
	{
		PlayerPrefs.SetInt ("BG", bBg ? 1 : 0);
		PlayerPrefs.SetInt ("EFF", bEff ? 1 : 0);
		PlayerPrefs.SetInt ("PUSH", bPush ? 1 : 0);
	}

	public static void GetSetting(out bool bBg, out bool bEff, out bool bPush)
	{

		int bgValue = PlayerPrefs.GetInt ("BG", 1);
		int effValue = PlayerPrefs.GetInt ("EFF", 1);
		int pushValue = PlayerPrefs.GetInt ("PUSH", 1);
		bBg = (bgValue == 1) ? true : false;
		bEff = (effValue == 1) ? true : false;
		bPush = (pushValue == 1) ? true : false;
	}
	
}
