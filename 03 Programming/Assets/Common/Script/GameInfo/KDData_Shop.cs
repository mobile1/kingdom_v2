﻿using UnityEngine;
using System.Collections.Generic;

public class KDData_Shop
{
	public List<ShopData> cashList {get;set;}
	public List<ShopData> coinList {get;set;}
	public List<ShopData> ticketList {get;set;}
}

public class ShopData
{
	public int index {get;set;}
	public int number {get;set;}
	public int addNum{get;set;}
	public int price {get;set;}
	public int sale {get;set;}
}